default['gitlab-ceph']['client-node']['cephfs'] = [{'mountpoint'=>'/var/opt/gitlab/git-data-ceph', 'device'=>'10.42.1.11:6789,10.42.1.12:6789,10.42.1.13:6789:/', 'options'=>'name=admin,secretfile=/etc/ceph/admin.secret,rsize=5242880,noatime,_netdev'}]
default['gitlab-ceph']['client-node']['admin_secret'] = "From attributes"
default['gitlab-ceph']['client-node']['chef_vault'] = nil
default['gitlab-ceph']['server-node']['fsid'] = "that's secret"
default['gitlab-ceph']['server-node']['mon_initial_shortname'] = nil
default['gitlab-ceph']['server-node']['mon_initial_ip'] = nil
default['gitlab-ceph']['server-node']['public_network'] = nil
default['gitlab-ceph']['server-node']['cluster_network'] = nil
default['gitlab-ceph']['server-node']['mon_cluster_log_to_syslog'] = nil
default['gitlab-ceph']['server-node']['mds_bal_frag'] = "true"
default['gitlab-ceph']['server-node']['mds_cache_size'] = "1000000"
default['gitlab-ceph']['server-node']['mds_log_max_expiring'] = "20"
default['gitlab-ceph']['server-node']['osd_op_threads'] = "4"
default['gitlab-ceph']['server-node']['osd_disk_threads'] = "1"
default['gitlab-ceph']['server-node']['osd_fs_op_threads'] = "2"
default['gitlab-ceph']['server-node']['osd_fs_que_max_ops'] = "500"
