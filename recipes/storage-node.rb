# Take the disk objects specified in a storage nodes profile
# and create an LVM object from it


# Query the chef server for the machiens role to double check

# pull the volume group and disks out of the node profile

# create the LVM Group

# ensure that they're in the 

include_recipe 'lvm::default'
include_recipe 'ceph-chef::default'

lvm_volume_group 'vg01' do
    physical_volumes ['/dev/sdc', '/dev/sdd']
    logical_volume 'data01' do
        size        '100%VG'
        filesystem  'xfs'
    end

end

lvm_volume_group 'vg02' do
    physical_volumes ['/dev/sdc', '/dev/sdd']
    logical_volume 'data02' do
        size        '100%VG'
        filesystem  'xfs'
    end

end

lvm_volume_group 'vg03' do
    physical_volumes ['/dev/sdc', '/dev/sdd']
    logical_volume 'data03' do
        size        '100%VG'
        filesystem  'xfs'
    end

end

lvm_volume_group 'vg04' do
    physical_volumes ['/dev/sdc', '/dev/sdd']
    logical_volume 'data04' do
        size        '100%VG'
        filesystem  'xfs'
    end

end

