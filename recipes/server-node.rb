# Perform the needful for client node setup and installation
# include_recipe 'ceph-chef::default'

include_recipe 'gitlab-vault'

server_node_conf = GitLab::Vault.get(node, 'gitlab-ceph', 'server-node')

template "/etc/ceph/ceph.conf" do
    source 'ceph.conf.erb'
    owner 'root'
    group 'root'
    mode 0600
    variables(server_node_conf)
end

template "/etc/rc.local" do
    source 'rc.local.erb'
    owner 'root'
    group 'root'
    mode 0755
end

