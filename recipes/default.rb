#
# Cookbook Name:: gitlab-ceph
# Recipe:: default
#
# Copyright (c) 2016 GitLab Inc, All Rights Reserved.

include_recipe 'gitlab-ceph::repository'

cookbook_file '/usr/local/bin/cephinfo' do
    source 'cephinfo'
    owner 'root'
    group 'root'
    mode '0755'
    action :create
end

cookbook_file '/usr/local/bin/ceph_scrub_summary' do
    source 'ceph_scrub_summary'
    owner 'root'
    group 'root'
    mode '0755'
    action :create
end

cookbook_file '/usr/local/bin/ceph_health_cron' do
    source 'ceph_health_cron'
    owner 'root'
    group 'root'
    mode '0755'
    action :create
end

cookbook_file '/usr/local/bin/ceph_gentle_reweight' do
    source 'ceph_gentle_reweight'
    owner 'root'
    group 'root'
    mode '0755'
    action :create
end
