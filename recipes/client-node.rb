# Perform the needful for client node setup and installation
# include_recipe 'ceph-chef::default'

include_recipe 'gitlab-vault'

client_node_conf = GitLab::Vault.get(node, 'gitlab-ceph', 'client-node')

apt_package 'ceph-fs-common' do
    action :upgrade
end

template "/etc/ceph/admin.secret" do
    source 'admin.secret.erb'
    owner 'root'
    group 'root'
    mode 0600
    variables(admin_secret: client_node_conf['admin_secret'])
end

node['gitlab-ceph']['client-node']['cephfs'].each do | cephfs |
  mount cephfs['mountpoint'] do
    device "#{cephfs['device']}"
    device_type 'device'
    fstype 'ceph'
    options "#{cephfs['options']}"
    action :enable # This will create an /etc/fstab entry
  end
end
